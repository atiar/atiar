package entity;

import entity.OrderEntity;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2013-05-25T19:50:58")
@StaticMetamodel(CustomerEntity.class)
public class CustomerEntity_ { 

    public static volatile SingularAttribute<CustomerEntity, Long> id;
    public static volatile SingularAttribute<CustomerEntity, String> phone;
    public static volatile SingularAttribute<CustomerEntity, String> email;
    public static volatile SingularAttribute<CustomerEntity, String> address;
    public static volatile SingularAttribute<CustomerEntity, String> name;
    public static volatile ListAttribute<CustomerEntity, OrderEntity> orders;

}