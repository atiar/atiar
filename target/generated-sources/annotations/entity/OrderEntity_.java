package entity;

import entity.CustomerEntity;
import entity.ProductEntity;
import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2013-05-25T19:50:58")
@StaticMetamodel(OrderEntity.class)
public class OrderEntity_ { 

    public static volatile SingularAttribute<OrderEntity, Long> id;
    public static volatile SingularAttribute<OrderEntity, ProductEntity> product;
    public static volatile SingularAttribute<OrderEntity, Integer> count;
    public static volatile SingularAttribute<OrderEntity, Date> datetime;
    public static volatile SingularAttribute<OrderEntity, CustomerEntity> customer;

}