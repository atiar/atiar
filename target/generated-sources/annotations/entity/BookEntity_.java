package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2013-05-25T19:50:58")
@StaticMetamodel(BookEntity.class)
public class BookEntity_ extends ProductEntity_ {

    public static volatile SingularAttribute<BookEntity, String> author;
    public static volatile SingularAttribute<BookEntity, String> isbn;

}