package entity;

import entity.OrderEntity;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2013-05-25T19:50:58")
@StaticMetamodel(ProductEntity.class)
public abstract class ProductEntity_ { 

    public static volatile SingularAttribute<ProductEntity, Long> id;
    public static volatile SingularAttribute<ProductEntity, String> title;
    public static volatile SingularAttribute<ProductEntity, Double> price;
    public static volatile SingularAttribute<ProductEntity, Integer> count;
    public static volatile SingularAttribute<ProductEntity, String> description;
    public static volatile ListAttribute<ProductEntity, OrderEntity> orders;
    public static volatile SingularAttribute<ProductEntity, String> type;

}