/*
 * Atiar Khan, s0217325
 */
package ejb;

import entity.ProductEntity;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "product")
@SessionScoped
public class ProductManagedBean implements Serializable{
    @EJB
    private SessionBean sessionBean;
    
    public List<ProductEntity> getProducts() {
        return sessionBean.getProducts();
    }
}
