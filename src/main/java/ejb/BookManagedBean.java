/*
 * Atiar Khan, s0217325
 */
package ejb;

import entity.BookEntity;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "book")
@SessionScoped
public class BookManagedBean implements Serializable {

    @EJB
    private SessionBean sessionBean;
    private BookEntity book;
    private String searchISBN;
    private List<BookEntity> books;
    private List<BookEntity> foundbooks;

    public List<BookEntity> getFoundbooks() {
        return foundbooks;
    }

    public void setBook(BookEntity book) {
        this.book = book;
    }

    public void setBooks(List<BookEntity> books) {
        this.books = books;
    }

    public void setFoundbooks(List<BookEntity> foundbooks) {
        this.foundbooks = foundbooks;
    }

    public String getSearchISBN() {
        return searchISBN;
    }

    public void setSearchISBN(String searchISBN) {
        this.searchISBN = searchISBN;
    }

    public List<BookEntity> getBooks() {
        if (books == null) {
            books = sessionBean.getBooks();
        }
        return books;
    }

    public BookEntity getBook() {
        if (book == null) {
            book = new BookEntity();
        }
        return book;
    }

    public String prepareSearch() {
        foundbooks = sessionBean.getBooksByISBN(searchISBN);
        searchISBN = "";
        return "Found?faces-redirect=true";
    }

    public String prepareList() {
        books = sessionBean.getBooks();
        return "List?faces-redirect=true";
    }

    public String prepareListFromMainPage() {
        return "book/" + prepareList();
    }

    public String prepareCreate() {
        book = new BookEntity();
        return "Create?faces-redirect=true";
    }

    public String prepareCreateFromMainPage() {
        return "book/" + prepareCreate();
    }

    public String create() {
        sessionBean.save(book);
        FacesMessage fm = new FacesMessage("Successfully created book: " + book.getTitle());
        FacesContext.getCurrentInstance().addMessage("Success", fm);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        return prepareList();
    }

    public String prepareView(Object selected) {
        foundbooks = new ArrayList<BookEntity>();
        foundbooks.add((BookEntity) selected);
        return "Found?faces-redirect=true";
    }

    public void prepareViewFromOrderPage(Object selected) {
        prepareView(selected);
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/book/Found.jsf");
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mainPage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath());
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public BookManagedBean() {
    }
}
