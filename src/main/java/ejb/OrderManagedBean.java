/*
 * Atiar Khan, s0217325
 */
package ejb;

import entity.OrderEntity;
import entity.ProductEntity;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "order")
@SessionScoped
public class OrderManagedBean implements Serializable {

    @EJB
    private SessionBean sessionBean;
    private OrderEntity order;
    private Long searchId = null;
    private List<OrderEntity> orders;
    private List<OrderEntity> foundorders;
    private Long customerId;
    private Long productId;

    public List<OrderEntity> getFoundorders() {
        return foundorders;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public void setOrders(List<OrderEntity> orders) {
        this.orders = orders;
    }

    public void setFoundorders(List<OrderEntity> foundorders) {
        this.foundorders = foundorders;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getSearchId() {
        return searchId;
    }

    public void setSearchId(Long searchId) {
        this.searchId = searchId;
    }

    public List<OrderEntity> getOrders() {
        if (orders == null) {
            orders = sessionBean.getOrders();
        }
        return orders;
    }

    public OrderEntity getOrder() {
        if (order == null) {
            order = new OrderEntity();
        }
        return order;
    }

    public Double totalPrice(Object selected) {
        BigDecimal result = BigDecimal.valueOf(((OrderEntity)selected).getCount());
        result = result.multiply(BigDecimal.valueOf(((OrderEntity)selected).getProduct().getPrice()));
        result = result.setScale(2);
        return (result.doubleValue());
    }

    public String prepareSearch() {
        foundorders = sessionBean.getOrdersById(searchId);
        searchId = null;
        return "Found?faces-redirect=true";
    }

    public String prepareList() {
        orders = sessionBean.getOrders();
        return "List?faces-redirect=true";
    }

    public String prepareListFromMainPage() {
        return "order/" + prepareList();
    }

    public String prepareCreate() {
        order = new OrderEntity();
        return "Create?faces-redirect=true";
    }

    public String prepareCreateFromMainPage() {
        return "order/" + prepareCreate();
    }

    public String create() {
        ProductEntity product = sessionBean.getProductById(productId);
        if (product.getCount() >= order.getCount()) {
            product.setCount(product.getCount() - order.getCount());
            sessionBean.update(product);
            order.setCustomer(sessionBean.getCustomerById(customerId));
            order.setProduct(sessionBean.getProductById(productId));
            order.setDatetime(new Date(System.currentTimeMillis()));
            sessionBean.save(order);
            FacesMessage fm = new FacesMessage("Successfully created order for "+order.getCustomer().getName());
            FacesContext.getCurrentInstance().addMessage("Success", fm);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return prepareList();
        } else {
            FacesMessage fm = new FacesMessage("Maximum count allowed for this item is "+product.getCount());
            FacesContext.getCurrentInstance().addMessage("Error", fm);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            order.setCount(product.getCount());
            return "Create?faces-redirect=true";
        }
    }

    public String delete(Object selected) {
        order = (OrderEntity) selected;
        ProductEntity product = order.getProduct();
        product.setCount(product.getCount() + order.getCount());
        sessionBean.update(product);
        sessionBean.delete(order);
        return prepareList();
    }

    public String prepareView(Object selected) {
        foundorders = new ArrayList<OrderEntity>();
        foundorders.add((OrderEntity) selected);
        return "Found?faces-redirect=true";
    }
    public void orderPage() {
       prepareList();
       FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath()+"/order/List.jsf");
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public void mainPage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath());
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public OrderManagedBean() {
    }
}
