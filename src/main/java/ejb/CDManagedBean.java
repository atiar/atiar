/*
 * Atiar Khan, s0217325
 */
package ejb;

import entity.CDEntity;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "cd")
@SessionScoped
public class CDManagedBean implements Serializable {

    @EJB
    private SessionBean sessionBean;
    private CDEntity cd;
    private String searchTitle;
    private List<CDEntity> cds;
    private List<CDEntity> foundcds;

    public CDEntity getCd() {
        return cd;
    }

    public List<CDEntity> getCds() {
        return cds;
    }

    public List<CDEntity> getFoundcds() {
        return foundcds;
    }

    public void setCd(CDEntity cd) {
        this.cd = cd;
    }

    public void setCds(List<CDEntity> cds) {
        this.cds = cds;
    }

    public void setFoundcds(List<CDEntity> foundcds) {
        this.foundcds = foundcds;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }

    public List<CDEntity> getCDs() {
        if (cds == null) {
            cds = sessionBean.getCDs();
        }
        return cds;
    }

    public CDEntity getCD() {
        if (cd == null) {
            cd = new CDEntity();
        }
        return cd;
    }

    public String prepareSearch() {
        foundcds = sessionBean.getCDsByTitle(searchTitle);
        searchTitle = "";
        return "Found?faces-redirect=true";
    }

    public String prepareList() {
        cds = sessionBean.getCDs();
        return "List?faces-redirect=true";
    }

    public String prepareListFromMainPage() {
        return "cd/" + prepareList();
    }

    public String prepareCreate() {
        cd = new CDEntity();
        return "Create?faces-redirect=true";
    }

    public String prepareCreateFromMainPage() {
        return "cd/" + prepareCreate();
    }

    public String create() {
        sessionBean.save(cd);
        FacesMessage fm = new FacesMessage("Successfully created CD: " + cd.getTitle());
        FacesContext.getCurrentInstance().addMessage("Success", fm);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        return prepareList();
    }

    public String prepareView(Object selected) {
        foundcds = new ArrayList<CDEntity>();
        foundcds.add((CDEntity) selected);
        return "Found?faces-redirect=true";
    }

    public void prepareViewFromOrderPage(Object selected) {
        prepareView(selected);
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/cd/Found.jsf" );
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mainPage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath());
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public CDManagedBean() {
    }
}
