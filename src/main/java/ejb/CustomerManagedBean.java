/*
* Atiar Khan, s0217325
 */
package ejb;

import entity.CustomerEntity;
import entity.OrderEntity;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "customer")
@SessionScoped
public class CustomerManagedBean implements Serializable {

    @EJB
    private SessionBean sessionBean;
    private CustomerEntity customer;
    private String searchName;
    private List<CustomerEntity> customers;
    private List<CustomerEntity> foundcustomers;

    public List<CustomerEntity> getFoundcustomers() {
        return foundcustomers;
    }

    public void setFoundcustomers(List<CustomerEntity> foundcustomers) {
        this.foundcustomers = foundcustomers;
    }
    
    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public void setCustomers(List<CustomerEntity> customers) {
        this.customers = customers;
    }

    public List<CustomerEntity> getCustomers() {
        if (customers == null) {
            customers = sessionBean.getCustomers();
        }
        return customers;
    }

    public CustomerEntity getCustomer() {
        if (customer == null) {
            customer = new CustomerEntity();
        }
        return customer;
    }

    public Double totalPrice(Object selected) {
        OrderEntity order = (OrderEntity) selected;
        BigDecimal result = BigDecimal.valueOf(order.getCount());
        result = result.multiply(BigDecimal.valueOf(order.getProduct().getPrice()));
        result = result.setScale(2);
        return (result.doubleValue());
    }

    public String prepareSearch() {
        foundcustomers = sessionBean.getCustomersByName(searchName);
        searchName = "";
        return "Found?faces-redirect=true";
    }

    public String prepareList() {
        customers = sessionBean.getCustomers();
        return "List?faces-redirect=true";
    }

    public String prepareListFromMainPage() {
        return "customer/" + prepareList();
    }

    public String prepareCreate() {
        customer = new CustomerEntity();
        return "Create?faces-redirect=true";
    }

    public String prepareCreateFromMainPage() {
        return "customer/" + prepareCreate();
    }

    public String create() {
        sessionBean.save(customer);
        FacesMessage fm = new FacesMessage("Successfully created the customer: " + customer.getName());
        FacesContext.getCurrentInstance().addMessage("Success", fm);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        return prepareList();
    }

    public String prepareView(Object selected) {
        customer = (CustomerEntity) selected;
        return "View?faces-redirect=true";
    }

    public void prepareViewFromOrderPage(Object selected) {
        prepareView(selected);
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/customer/View.jsf");
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mainPage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath());
        } catch (IOException ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public CustomerManagedBean() {
    }
}
