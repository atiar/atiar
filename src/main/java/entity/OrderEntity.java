/*
 * Atiar Khan, s0217325
 */
package entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="Orders")
@NamedQueries(
        {
            @NamedQuery(name = "Order.findAll", query = "SELECT o FROM OrderEntity o"),
            @NamedQuery(name = "Order.findByOrdersId", query = "SELECT o FROM OrderEntity o WHERE o.id = :id")
        })
public class OrderEntity implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    @Column(name = "count")
    protected Integer count;
    @Column(name = "datetime")
    protected Date datetime;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer")
    protected CustomerEntity customer;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product")
    private ProductEntity product;

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }
    
    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
    
    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }
}
